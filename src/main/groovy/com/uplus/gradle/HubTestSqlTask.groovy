package com.uplus.gradle

import com.medialog.env.GProp
import com.nocwriter.runsql.gradle.DriverClassLoaderBuilder
import com.nocwriter.runsql.gradle.RunSQLProperties
import com.nocwriter.runsql.gradle.ScriptObject
import com.nocwriter.runsql.gradle.ScriptsReader
import com.nocwriter.runsql.jdbc.JdbcUtils
import com.nocwriter.runsql.script.ScriptParser
import com.uplus.type.SQLProperties
import groovy.sql.Sql
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

import java.sql.SQLException

public class HubTestSqlTask extends DefaultTask {

    @Input
    String description

    private final SQLProperties query = new SQLProperties();

    public void query(Action<SQLProperties> action) {
        action.execute(query);
    }

    @TaskAction
    public void execute() throws SQLException {

        println "HubTestSql " + description

        GProp gprop = new GProp()
        ClassLoader jdbcClassLoader = DriverClassLoaderBuilder.createDriversClassLoader(getProject());
        JdbcUtils.registerDriver(jdbcClassLoader, gprop.medialog.db.driver);

        def sql = new Sql(JdbcUtils.openJDBCConnection(gprop.medialog.db.user, gprop.medialog.db.pass, gprop.medialog.db.url))

        if(query.select != "") {
            println "select  ---" + query.select
            def rows = []

            sql.eachRow(query.select) {
                println "----"
                println it["name"]
                println "----"
                //rows << it.toRowResult()
            }
        }

        if(query.insert != "") {
            def upcount =  sql.executeUpdate query.insert
            println upcount + " row inserted"
        }

        if(query.insert != ""){
            println "insert  ---"
            def upcount =  sql.executeUpdate query.insert
            println upcount + " row inserted"
        }

        query.insertList.collect {
            println "insertList ---"
            def upcount =  sql.executeUpdate it.value
            println upcount + " row inserted"
        }

        if(query.insert != ""){
            println "update  ---"
            def upcount =  sql.executeUpdate query.update
            println upcount + " row inserted"
        }

        query.updateList.collect {
            println "updatetList ---"
            def upcount =  sql.executeUpdate it.value
            println upcount + " row updated"
        }

        sql.close()
    }

}