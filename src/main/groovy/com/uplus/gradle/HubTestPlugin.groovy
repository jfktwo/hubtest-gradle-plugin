package com.uplus.gradle

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Gradle task register
 *
 * @author Jatu
 * @since 2022/06/14
 */

public class HubTestPlugin implements Plugin<Project>  {

    private static final Logger logger = LoggerFactory.getLogger(HubTestPlugin.class);

    @Override
    public void apply(Project project) {
        logger.info("Plugin {} created successfully for project {}", getClass().getSimpleName(), project.getName());

//        project.task("insertJatu", type: HubTestSqlTask);

        project.task("insertJatu", type: InsertTask);
//        project.getTasks().register("hthelp", HubTestHelpTask.class);
        project.getTasks().register("insertPlugin", InsertTask.class);
//        project.getTasks().register("htinsert", HubTestInsertTask.class);






    }
}



