package com.uplus.gradle

import com.medialog.tool.Tail
import com.uplus.type.SQLProperties
import com.uplus.type.TailProperties
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

import java.sql.SQLException

class HubTestTailTask extends DefaultTask {

    @Input
    String description

    private final TailProperties tail = new TailProperties();

    public void tail(Action<TailProperties> action) {
        action.execute(tail);
    }

//    public void config(Action<RunSQLProperties> configAction) {
//        configAction.execute(this.props);
//    }

    @TaskAction
    void execute() throws SQLException {

        println "tail " + description + tail.fileName;

        println new File(tail.fileName).exists();
        Tail.runTail(tail.fileName)

    }
}
