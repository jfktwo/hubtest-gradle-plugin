package com.uplus.gradle

import com.medialog.env.GProp
import com.nocwriter.runsql.gradle.DriverClassLoaderBuilder
import com.nocwriter.runsql.gradle.RunSQLProperties
import com.nocwriter.runsql.gradle.ScriptObject
import com.nocwriter.runsql.gradle.ScriptsReader
import com.nocwriter.runsql.jdbc.JdbcUtils
import com.nocwriter.runsql.script.ScriptParser
import groovy.sql.Sql
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import com.medialog.bin.db.insert

import java.sql.SQLException;

/**
 * Gradle task helpMessage
 *
 * @author Jatu
 * @since 2022/06/14
 */

public class HubTestHelpTask extends DefaultTask {


    public void printHelp() {

            //나중에 resource에 있는 help파일을 열어서 사용 하도록 한다.
        println "httest help"
       insert.main();

    }


    @TaskAction
    public void execute() throws SQLException {
        println "httest help"
        println "httest help"
        println "httest help"
        println "httest help"
        println "httest help"
        insert.main();
    }
}


public class InsertTask extends DefaultTask {

    public void printHelp() {

        //나중에 resource에 있는 help파일을 열어서 사용 하도록 한다.
        println "hub Test help"
        insert.main();

    }
    private final RunSQLProperties props = new RunSQLProperties();

    public void config(Action<RunSQLProperties> configAction) {
        configAction.execute(this.props);
    }

    @TaskAction
    public void execute() throws SQLException {

        println "insert start"
        GProp gprop = new GProp()
        ClassLoader jdbcClassLoader = DriverClassLoaderBuilder.createDriversClassLoader(getProject());
        JdbcUtils.registerDriver(jdbcClassLoader, gprop.medialog.db.driver);

        def sql = new Sql(JdbcUtils.openJDBCConnection(gprop.medialog.db.user, gprop.medialog.db.pass, gprop.medialog.db.url))
        println gprop.medialog.db.sql.insert
        gprop.medialog.db.sql.insert.collect {
            println it.value
            def upcount =  sql.executeUpdate it.value
            println upcount + " row inserted"
        }
        sql.close()
    }
}



