package com.uplus.gradle

import com.uplus.type.CodeProperties
import com.uplus.type.UiProperties
import groovy.swing.SwingBuilder
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction


import java.sql.SQLException

class HubTestCodeTask extends DefaultTask {

    @Input
    String description


    private final CodeProperties code = new CodeProperties();

    public void code(Action<CodeProperties> action) {
        action.execute(code);
    }

//    public void config(Action<RunSQLProperties> configAction) {
//        configAction.execute(this.props);
//    }

    @TaskAction
    void execute() throws Exception {
        println "code " + description ;

        if(code.groovyScript != ""){
            GroovyShell shell = new GroovyShell();
            shell.evaluate(code.groovyScript);
        }

        if(code.javaScript != ""){
            GroovyShell shell = new GroovyShell();
            shell.evaluate(code.javaScript);
        }

        if(code.psScript != ""){
//            def process = code.psScript.execute()
//            def process = "echo 'test'".execute()
//            print "Output: " + process.text
//            print "Exit code: " + process.exitValue()

            def sout = new StringBuilder(), serr = new StringBuilder()
            def shellCommand= code.psScript //your shell command here
            Process proc = shellCommand.split().execute()
            proc.consumeProcessOutput(sout, serr)
            proc.waitForOrKill(100000L)

        }

        if(code.bashScript != ""){
            def process = code.bashScript.execute()
            print "Output: " + process.text
            print "Exit code: " + process.exitValue()
        }



    }
}
