package com.uplus.gradle

import com.medialog.tool.Tail
import com.uplus.type.TailProperties
import com.uplus.type.UiProperties
import groovy.swing.SwingBuilder
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import java.awt.BorderLayout

import java.sql.SQLException

class HubTestUiTask extends DefaultTask {

    @Input
    String description

    private final UiProperties ui = new UiProperties();

    public void ui(Action<UiProperties> action) {
        action.execute(ui);
    }

//    public void config(Action<RunSQLProperties> configAction) {
//        configAction.execute(this.props);
//    }

    @TaskAction
    void execute() throws SQLException {
        println "ui " + description ;
        println ui.uiScript;
        println ui.title;

        def count = 0
        new SwingBuilder().edt {
            frame(title: "Frame", size: [300, 300], show: true) {
                borderLayout()
                def textlabel = label(text: 'Click the button!', constraints: BorderLayout.NORTH)
                button(text:'Click Me',
                        actionPerformed: {count++; textlabel.text = "Clicked ${count} time(s)."; println "clicked"}, constraints:BorderLayout.SOUTH)
            }
        }

    }
}
