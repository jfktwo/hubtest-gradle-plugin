package com.uplus.gradle


import com.uplus.type.SQLProperties
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

import java.sql.SQLException

class HubTestTask extends DefaultTask {

    @Input
    String description

    private final SQLProperties query = new SQLProperties();

    public void query(Action<SQLProperties> action) {
        action.execute(query);
    }

//    public void config(Action<RunSQLProperties> configAction) {
//        configAction.execute(this.props);
//    }


    @TaskAction
    void execute() throws SQLException {

        println "HubTestTask" + description

    }

}
