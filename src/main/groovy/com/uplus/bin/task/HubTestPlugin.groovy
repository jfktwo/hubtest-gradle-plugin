package com.uplus.bin.task

import com.medialog.env.GProp
import groovy.sql.Sql
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.provider.Property;

abstract class HubTestExtension {
    abstract Property<String> getMessage()
    abstract Property<String> getGreeter()

    HubTestExtension() {
        message.convention('complete')
        greeter.convention('Hello from HubtestPlugin')
    }
}

class HubTestPlugin implements Plugin<Project> {
    void apply(Project project) {

        def extension = project.extensions.create('hubtest', HubTestExtension)
        project.task('insert') {
//            insert.main();

            GProp gprop = new GProp()

            def sql = Sql.newInstance(gprop.medialog.db.url, gprop.medialog.db.user, gprop.medialog.db.pass, gprop.medialog.db.driver)
            gprop.medialog.db.sql.insert.collect {
                println it.value
                def upcount =  sql.executeUpdate it.value
                println upcount + " row inserted"
            }
            sql.close()
            doLast {
                println "insert " + extension.getMessage();
            }
        }

        project.task('update') {
//            update.main();
            doLast {
                println "update " + extension.getMessage();
            }
        }

        project.task('select') {
//            select.main();
            doLast {
                println "select " + extension.getMessage();
            }

        }
    }
}