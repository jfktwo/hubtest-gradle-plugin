package com.medialog.tool

class Tail {
	
	boolean stop = false
	
	  public void stop () {
		stop = true
	  }
	
	  public void tail (String fileName, Closure c) {
		  
		File file = new File(fileName)  
		  
		def runnable = {
		 def reader
	
		  try {
			reader = file.newReader()
			reader.skip(file.length())
	
			def line
	
			while (!stop) {
			  line = reader.readLine()
			  if (line) {
				c.call(line)
			  }
			  else {
				Thread.currentThread().sleep(1000)
			  }
			}
	
		  }
		  finally {
			reader?.close()
		  }
		} as Runnable
	
		def t = new Thread(runnable)
		t.start()
	 }
	 
	 public static runTail(String[] args) {
		 
		 def reader = new Tail()
		 if(args.length < 1) reader.useage();
		 else {
			 if(args.length == 1) {
				 reader.tail(args[0]) { println it  }
			 } else if(args.length == 2){
				 reader.tail(args[0]) { if(it.indexOf(args[1]) > 0) println it }
			 }
		 }
	 }
	 
	
	 public void useage() {
		 println "useage : ${command} filename (grepStr) "
	 }

	static void main(args) {
		// TODO Auto-generated method stub
		
				
		def reader = new Tail()		
		if(args.length < 1) reader.useage();
		else {
			if(args.length == 1) {
				reader.tail(args[0]) { println it  }
			} else if(args.length == 2){
				reader.tail(args[0]) { if(it.indexOf(args[1]) > 0) println it }
			}
		}	
		
	}
}
