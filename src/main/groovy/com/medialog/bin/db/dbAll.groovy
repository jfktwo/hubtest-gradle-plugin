package com.medialog.bin.db
/*
Groovy     (http://groovy.codehaus.org)
	Grails     (http://grails.org)
	Griffon    (http://griffon.codehaus.org)
	Gradle     (http://gradle.org)
	*/

import groovy.sql.Sql
import com.medialog.env.GProp

class insert {
	
		static void main(args) {
		
			GProp gprop = new GProp()
			
			def sql = Sql.newInstance(gprop.medialog.db.url, gprop.medialog.db.user, gprop.medialog.db.pass, gprop.medialog.db.driver)
			gprop.medialog.db.sql.insert.collect {
				println it.value
				def upcount =  sql.executeUpdate it.value
				println upcount + " row inserted" 
			}
			sql.close()
		}
				
}

//"select tr_phone, tr_msg from sc_tran"
//https://examples.javacodegeeks.com/jvm-languages/groovy/groovy-array-example/
class select {
		static void main(args) {
			GProp gprop = new GProp()
			def sql = Sql.newInstance(gprop.medialog.db.url, gprop.medialog.db.user, gprop.medialog.db.pass, gprop.medialog.db.driver)
			gprop.medialog.db.sql.select.collect {
				sql.eachRow(it) { row -> println row.toString() }
			}
			sql.close()
		}
}


class update {
	static void main(args) {
		GProp gprop = new GProp()
		def sql = Sql.newInstance(gprop.medialog.db.url, gprop.medialog.db.user, gprop.medialog.db.pass, gprop.medialog.db.driver)
		gprop.medialog.db.sql.update.collect {
			println it.value
			def upcount =  sql.executeUpdate it.value
			println upcount + " row updated"
		}
		sql.close()
	}
}
