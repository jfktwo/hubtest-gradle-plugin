HUBTEST Gradle plugin
=====================

Gradle plugin for HUBTEST  scripts.


How to use this plugin?
-----------------------
셋팅 방법:

For Groovy-based DSL:

    plugins {
        id "com.uplus.hubtest" version "1.0.0"
    }

For Kotlin-based DSL:

    plugins {
        id("com.uplus.hubtest") version ("1.0.0")
    }
    
위와 같이 셋팅 하면 되나 DSL 심사통과 못함 추후 통과 시킨다면 위의 한줄만 셋팅하면됨:
통과 안됬므로 아래와 같이 jar파일 경로를 직접 설정 또는 사설 nexus 경로를 사용

    buildscript{
         dependencies {
            classpath files('../hubtest-gradle-plugin/build/libs/hubtest-gradle-plugin-1.0.0.jar')
        }
    }

플러그인이 셋팅한 task를 사용한다면:

    apply plugin : 'com.uplus.hubtest'

위와 같이 hubtest로 생성 할수 있는 task들을 만들었으나 tyep이 사용 효율성이 높아 사용하지 않음.


플러그인이 셋팅한 type만 사용한다면:

    apply from: 'hubtest-sql.gradle'


쿼리나 실행을 별도 gradle파일에 모아둠

    buildscript{
        dependencies {
            classpath files('../hubtest-gradle-plugin/build/libs/hubtest-gradle-plugin-1.0.0.jar')
        }
    }

        
    task tailHub(type: HubTestTail){
    group = "hubTest"
    description = "tail hub"
    tail {
    fileName = "c:/work/test.txt"
    }
    }
    
    task selectHub(type: HubTestSql) {
    group = "hubTest"
    description = "select hub"
    
        query {
            select = '''
                    select name from jatutest4
                    '''
        }
    }
    
    task createHub(type: HubTestSql) {
    group = "hubTest"
    println "testDataInsert"
    description = "some querylist"
    query {
    insert = '''
    create table jatutest4(name varchar(100))
    '''
    }
    }
    
    
    task insertHub(type: HubTestSql) {
    group = "hubTest"
    println "testDataInsert"
    description = "some querylist"
    query {
    insert = "insert into jatutest4(name) values( 'TT 한글메시지xxxx insert')"
    10.times{
    insertList.put(it,  '''
    insert into jatutest4(name)
    values( 'TT 한글메시지x  insertList'''+it+'''')
    ''')
    }
    update = '''			
    insert into jatutest4(name) values( 'TT 한글메시지xxxx update')
    '''
    updateList.put(0,
    '''
    insert into jatutest4(name) values( 'TT 한글메시지xxxx updateList1')
    ''')
    updateList.put(1,
    '''
    insert into jatutest4(name) values( 'TT 한글메시지xxyyy updateList2')
    ''')
    }
    }
    
    
    task uiHub(type: HubTestUi) {
    group = "hubtest"
    description = "ui hub"
    
        ui {
            title = "testProgram"
            uiScript = '''
    '''
    
        }
    }
    
    
    
    task codeGroovyHub(type: HubTestCode) {
    group = "hubtest"
    description = "code for hub"
    
        code {
            groovyScript = '''
    
    def test='testString groovyScript'
    println test
    
    class User {
    String username
    
        void bedtime() {
            long start = System.currentTimeMillis()
            println "Sleeping"
            sleep(5000) { e ->
                assert e in InterruptedException
                println "Yeah, yeah, I am awake..."
                true // stop on sleeping       
            }
            long slept = System.currentTimeMillis() - start
            println "Awake after $slept ms"
        }
    }
    
    def user = new User(username: 'mrhaki')
    
    // Run bedtime method in thread.
    def bedtime = Thread.start {
    user.bedtime()
    }
    
    def alarm = new Timer()
    alarm.runAfter(2000) {
    println "BEEP BEEP"
    // Interrupt thread with bedtime method.
    bedtime.interrupt()
    }
    
    bedtime.join()
    
    '''
    }
    
    }
    
    
    
    task codeJavaHub(type: HubTestCode) {
    group = "hubtest"
    description = "bash code for hub"
    
        code {
            title = "testProgram"
            javaScript = '''
    System.out.println("jatu code java");
    '''
    }
    }
    
    task codePsHub(type: HubTestCode) {
    group = "hubtest"
    description = "bash code for hub"
    
        code {
            title = "testProgram"
            psScript = '''
    echo "win shelltest"
    '''
    }
    }
    
    
    task codeBashHub(type: HubTestCode) {
    group = "hubtest"
    description = "bash code for hub"
    
        code {
            title = "testProgram"
            bashScript = '''
    #!/bin/bash
    
    start(){
    echo $1
    }
    
    for i in `ls -al`
    do
    start();
    done
    '''
    }
    }



Scripts
----------------




JDBC driver class name
----------------------

System requirements
-------------------
The plugin requires Java 8 and above and Gradle 6.9 or above.

License
-------
This plugin is licensed under [Apache License V2.0](LICENSE).

How to contact you
------------------

Contributors
------------
Thanks to @jatu for suggesting adjustments in favor of Java 8 compatability.

it maked base on com.nocwriter.runsql thank

https://plugins.gradle.org/u/jatu?tab=publishing

--
gradle pub key
.gradle/gradle.properties
gradle.publish.key=rLBlBNUtdn8PDvHKAB4yWJeSTjkjvJwh
gradle.publish.secret=gihxKL3fRPDl5of0kSg9TdbNZHQWZy4A